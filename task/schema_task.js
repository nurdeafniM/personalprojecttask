const mongoose= require('mongoose')
let Schema = mongoose.Schema
let schemaTask = new Schema({
    id: Number,
    title: String,
    description: String,
    dueDate: Date,
    comment: String,
    status:String
})
module.exports={schemaTask}