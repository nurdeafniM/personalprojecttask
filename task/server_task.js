'use strict';

const Hapi = require('@hapi/hapi');
const Inert = require('@hapi/inert');
const Bcrypt = require('bcrypt');
const Qs = require('qs');
const CatboxRedis = require('@hapi/catbox-redis');
const {getAllTask,getTaskById,postTask,updateById,deleteTask,getBySort}= require('./task_handler')


const users = {
    nurDe: {
        username: 'nurDe',
        password: '$2a$10$iqJSHD.BGr0E2IxQwYgJmeP3NvhPrXAeLSaGCj6IR/XU5QtjVu5Tm',   // 'secret' 
        name: 'Nur De Afni Melani',
        id: '2133d32a'
    }
};
const validate = async (request, username, password) => {
    const user = users[username];
    if (!user) {
        return { credentials: null, isValid: false };
    }
    const isValid = await Bcrypt.compare(password, user.password);
    const credentials = { id: user.id, name: user.name };
    return { isValid, credentials };
};


const init = async () => {

    const server = Hapi.server({
        port: 4000,
        host: 'localhost',
        query:{
            parser:(query) => Qs.parse(query)
        },
        cache: [
            {
                name: 'my_cache',
                provider: {
                    constructor: CatboxRedis,
                    options: {
                        partition : 'my_cached_data',
                        host: '127.0.0.1',
                        port: 6379,
                        database: 0
                    }
                }
            }
        ]
    });
    await server.register(require('@hapi/basic'));
    await server.register({
        plugin: require('hapi-pino'),
        options: { 
          prettyPrint: process.env.NODE_ENV !== 'production',
           stream:'./server.log',
           redact: ['req.headers.authorization']
        }
      })
    server.auth.strategy('simple', 'basic', { validate });
    await server.register(Inert)

    server.route({
        method: 'GET',
        path: '/tasks',
        handler: getAllTask
    })
    server.route({
        method: 'GET',
        path: '/task/{id}', 
        handler: getTaskById
    })
    server.route({
        method: 'POST',
        path: '/task',
        handler: postTask
    })
    server.route({
        method: 'PUT',
        path: '/task/{id}',
        handler: updateById
    })
    
    server.route({
        method: 'DELETE',
        path: '/task/{id}',
        handler: deleteTask
    })
    
    server.route({
        method: 'GET',
        path: '/task',
        handler: getBySort
    })
    process.on('unhandledRejection', (err) => {
        console.log(err)
        process.exit(1)
    })
     await server.start()
    console.log('Server running on %s', server.info.uri)
    return server
}

module.exports={init}