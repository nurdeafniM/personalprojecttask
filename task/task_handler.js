const schemaTask= require('./schema_task')
const mongoose= require('mongoose')
mongoose.connect('mongodb://localhost/dkatalis',{
    useUnifiedTopology: true,
    useNewUrlParser: true
});
const task= mongoose.model('tasks',schemaTask)

const getAllTask= async function (request, h) {
    return task.find();
}
const getTaskById = async (request, h)=>{
    let tasks =await task.find({id: parseInt(request.params.id)}).lean()
    return  tasks
}
const postTask= async function(request, h){
    const idTask= await task.aggregate([ 
        {
            $group:{
                _id:'max',
                maxId:{
                    $max:'$id',
                }, 
            }
        }
    ])
    let nextId=idTask[0].maxId+1
    Object.assign(request.payload,{id:nextId})
    await task.insertMany([request.payload])
    return h.response(request.payload).code(201)
}
const updateById = async function(request,h){
    if(req.dueDate){
        const result = await personalTask.findOneAndUpdate({id: request.params.id}, {date: req.date , dueTask: req.dueTask, status: "in-progress" }, { new: true })
        return h.response(result)
    }else{
        const result = await personalTask.findOneAndUpdate({id: request.params.id}, {date: req.date , dueTask: req.dueTask, status: "complete" }, { new: true })
        return h.response(result)
    }
}
const deleteTask = async (request, h) => {
    try {
        const result = await task.deleteOne({id: request.params.id})
        return h.response(result)
    } catch (error) {
        return h.response(error).code(500)
    }
}
const getBySort = async function (request, h) { //http://localhost:3686/personalTask/query?sort=id&limit=2&offset=0&filter[status][$eq]=complete
    const sorting = request.query.sort
    const limit = parseInt(request.query.limit)
    const offset = parseInt(request.query.offset)
    const filter = request.query.filter
    return await task.find(filter).sort(sorting).skip(offset).limit(limit)
}


module.exports={getAllTask,getTaskById,postTask,updateById,deleteTask,getBySort} 